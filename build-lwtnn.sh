#!/usr/bin/env bash

set -eu

mkdir lwtnn
(
    cd lwtnn
    git clone https://github.com/lwtnn/lwtnn.git
    mkdir build
    cd build
    cmake ../lwtnn
    make -j $(nproc)
    sudo make install
    sudo ldconfig
)
rm -rf lwtnn
